import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:seminar_demo/themes/app_theme.dart';
import 'package:seminar_demo/view_model/message_view_model.dart';
import 'package:seminar_demo/widget/text_field.dart';

import 'model/message_entity.dart';

class ChatScreen extends StatefulWidget {
  ChatScreen({Key? key, required this.userName}) : super(key: key);
  String userName;

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  TextEditingController _controller = TextEditingController();
  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
  }

  MessageViewModel viewModel = Get.put(MessageViewModel());

  Future scrollToTheEnd() async {
    if (!_scrollController.hasClients) return;
    await Future.delayed(Duration(milliseconds: 100));
    _scrollController.animateTo(_scrollController.position.maxScrollExtent,
        duration: const Duration(milliseconds: 300),
        curve: Curves.fastOutSlowIn);
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      scrollToTheEnd();
      return Scaffold(
        appBar: AppBar(
          title: const Text('SE1415 Chat room'),
        ),
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            children: [
              Expanded(
                  child: ListView.builder(
                controller: _scrollController,
                itemBuilder: (context, index) {
                  Message message = viewModel.messageList.value[index];
                  if (message.user == widget.userName) {
                    return rightMessage(message);
                  }
                  return leftMessage(message);
                },
                itemCount: viewModel.messageList.length,
              )),
              Container(
                margin: EdgeInsets.only(bottom: 20, top: 25),
                child: Row(
                  children: [
                    Container(
                      padding: EdgeInsets.only(right: 15),
                      width: MediaQuery.of(context).size.width * 0.68,
                      child: CustomTextField(controller: _controller),
                    ),
                    Expanded(
                      child: Container(),
                    ),
                    ElevatedButton.icon(
                        onPressed: () {
                          viewModel.sendMessage(
                              _controller.text, widget.userName);
                          _controller.text = '';
                          // scrollToTheEnd();
                        },
                        icon: Icon(Icons.send),
                        label: Text('SEND'))
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    });
  }

  Widget leftMessage(Message message) {
    return Container(
      padding: EdgeInsets.only(right: 70),
      child: Card(
          elevation: 0,
          color: AppTheme.notWhite,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Container(
            color: AppTheme.transparent,
            padding: EdgeInsets.only(left: 13, right: 13, top: 8, bottom: 13),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  message.user!,
                  style: AppTheme.buildCaption(color: AppTheme.deactivatedText),
                ),
                SizedBox(height: 5,),
                Text(
                  message.text!,
                  style: AppTheme.body1,
                ),
              ],
            ),
          )),
    );
  }

  Widget rightMessage(Message message) {
    return Container(
      padding: EdgeInsets.only(left: 70),
      child: Card(
          elevation: 0,
          color: Colors.blueAccent,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Container(
            color: AppTheme.transparent,
            padding: EdgeInsets.only(left: 13, right: 13, top: 8, bottom: 13),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  message.user!,
                  style: AppTheme.buildCaption(color: AppTheme.notWhite),
                ),
                SizedBox(height: 5,),
                Text(
                  message.text!,
                  style: AppTheme.buildBody1(color: AppTheme.nearlyWhite), textAlign: TextAlign.start,
                ),
              ],
            ),
          )),
    );
  }
}
