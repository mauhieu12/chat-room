import 'package:seminar_demo/model/message_entity.dart';

messageFromJson(Message data, Map<dynamic, dynamic> json) {
	if (json['text'] != null) {
		data.text = json['text'].toString();
	}
	if (json['user'] != null) {
		data.user = json['user'].toString();
	}
	return data;
}

Map<String, dynamic> messageToJson(Message entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['text'] = entity.text;
	data['user'] = entity.user;
	return data;
}