import 'package:flutter/material.dart';
import 'package:seminar_demo/chat_screen.dart';
import 'package:seminar_demo/widget/text_field.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: CustomTextField(
                  controller: _controller, labelText: 'Username'),
            ),
            ElevatedButton.icon(
                onPressed: () {
                  if (_controller.text.isEmpty) return;
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            ChatScreen(userName: _controller.text)),
                  );
                },
                icon: Icon(Icons.message),
                label: Text('Join chat room'))
          ],
        ),
      ),
    );
  }
}
