import 'package:seminar_demo/generated/json/base/json_convert_content.dart';

class Message with JsonConvert<Message> {
	String? text;
	String? user;

	Message();
	Message.build(this.text, this.user);
}
