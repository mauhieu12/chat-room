import 'dart:async';

import 'package:firebase_database/firebase_database.dart';
import 'package:get/get.dart';
import 'package:seminar_demo/model/message_entity.dart';
import 'package:seminar_demo/repository/message_repository.dart';

class MessageViewModel extends GetxController {
  final _messageRepository = MessageRepository();
  var messageList = List<Message>.empty().obs;

  @override
  void onInit() {
    messageList.clear();
    _messageRepository.listenNewMessage(
        onNewMessage: (message) => onNewMessage(message));
  }

  void onNewMessage(Message message) {
    messageList.add(message);
    print('On new message: ' + message.text!);
  }

  void sendMessage(String text, String user) {
    if (text.isEmpty) return;
    _messageRepository.sendMessage(Message.build(text, user));
    print('${user} sent message: ' + text);
  }

  void getMessage() {
    // message.text = text;
    _messageRepository.getMessage();
    // print('sent message ' + message.text!);
  }
}
