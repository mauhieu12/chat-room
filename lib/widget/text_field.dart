import 'package:flutter/material.dart';
import 'package:seminar_demo/themes/app_theme.dart';

Widget CustomTextField(
    {TextEditingController? controller,
    String? hintText,
    String? labelText,
    bool obscureText = false,
    bool? enable,
    Function(String text)? onTextChange,
    Icon? postfixIcon,
    Icon? prefixIcon,
    String? initialValue,
    TextInputAction? textInputAction}) {
  return TextFormField(
      onChanged: (value) {
        if (onTextChange != null) {
          onTextChange(value);
        }
      },
      enabled: enable,
      controller: controller,
      obscureText: obscureText,
      style: AppTheme.subtitle1,
      textInputAction: textInputAction,
      initialValue: initialValue,
      decoration: InputDecoration(
        fillColor: AppTheme.notWhite,
        focusColor: AppTheme.notWhite,
        filled: true,
        // prefixIcon: Icon(Icons.search),
        // icon: Icon(Icons.search_rounded),
        hintText: hintText,
        labelText: labelText,
        labelStyle: AppTheme.buildSubtitle1(color: AppTheme.deactivatedText),
        hintStyle: AppTheme.buildSubtitle1(color: AppTheme.deactivatedText),
        // contentPadding: EdgeInsets.only(left: 15, right: 15, top: 0, bottom: 0),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(7)),
          borderSide: BorderSide(
            color: AppTheme.primaryColor,
          ),
        ),
        enabledBorder: OutlineInputBorder(
          gapPadding: 0,
          borderRadius: BorderRadius.all(Radius.circular(7)),
          borderSide: BorderSide(
            color: AppTheme.primaryColor,
          ),
        ),
      ));
}
