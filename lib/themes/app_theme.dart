import 'package:flutter/material.dart';

class AppTheme {
  AppTheme._();

  static MaterialStateProperty<Color> getMaterialColor(Color color) {
    return MaterialStateProperty.all<Color>(color);
  }

  static MaterialColor createMaterialColor(Color color) {
    List strengths = <double>[.05];
    final swatch = <int, Color>{};
    final int r = color.red, g = color.green, b = color.blue;

    for (int i = 1; i < 10; i++) {
      strengths.add(0.1 * i);
    }
    strengths.forEach((strength) {
      final double ds = 0.5 - strength;
      swatch[(strength * 1000).round()] = Color.fromRGBO(
        r + ((ds < 0 ? r : (255 - r)) * ds).round(),
        g + ((ds < 0 ? g : (255 - g)) * ds).round(),
        b + ((ds < 0 ? b : (255 - b)) * ds).round(),
        1,
      );
    });
    return MaterialColor(color.value, swatch);
  }

  static const Color transparent = Colors.transparent;
  static const Color background = Color(0xFFFFFFFF);

  static const Color primaryColor = Color(0xFF1A434E);
  static const Color secondaryColor = Color(0xFFD2C2F7);
  static const Color secondaryBackgroundColor = Color(0xFFF0F1FC);

  static const Color notWhite = Color(0xFFECEDF3);
  static const Color nearlyWhite = Color(0xFFFFFFFF);
  static const Color nearlyBlue = Color(0xFFE1F0FF);
  static const Color nearlyBlack = Color(0xFF314E4E);
  static const Color grey = Color(0xFF3A5160);
  static const Color dark_grey = Color(0xFF313A44);
  static const Color nearly_grey = Color(0x8F6F7B77);
  static const Color green = Color(0xFFFFDED4);

  static const Color darkText = Color(0xFF253840);
  static const Color darkerText = Color(0xFF17262A);
  static const Color lightText = Color(0xFF5C7B89);
  static const Color deactivatedText = Color(0xBC767676);
  static const Color dismissibleBackground = Color(0xFF364A54);
  static const Color chipBackground = Color(0xFFEEF1F3);
  static const Color spacer = Color(0xFFF2F2F2);
  static const Color barrierColor = Color(0xC9F0F1FC);

  static const Color blue = Color(0xFFB1D5FA);

  static const String fontName = 'WorkSans';
  static const TextTheme textTheme = TextTheme(
    headline4: headline4,
    headline5: headline,
    headline6: title,
    subtitle2: subtitle,
    bodyText1: body2,
    bodyText2: body1,
    caption: caption,
  );

  static const TextStyle appBarTitle = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w700,
    fontSize: 28,
    letterSpacing: 1.2,
    color: darkerText,
  );

  static const TextStyle headline4 = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w500,
    fontSize: 34,
    letterSpacing: 0.25,
    color: darkerText,
  );

  static const TextStyle headline5 = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w600,
    fontSize: 26,
    letterSpacing: 0.0,
    // height: 0.9,
    color: darkerText,
  );

  static const TextStyle headline6 = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w500,
    fontSize: 20,
    letterSpacing: 0.15,
    color: darkerText,
  );

  static TextStyle buildHeadline6({Color color = darkText}) {
    return TextStyle(
      fontFamily: fontName,
      fontWeight: FontWeight.w500,
      fontSize: 20,
      letterSpacing: 0.15,
      color: color,
    );
  }

  static const TextStyle subtitle1 = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w500,
    fontSize: 16,
    letterSpacing: 0.15,
    color: darkerText,
  );

  static const TextStyle subtitle1Light = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w500,
    fontSize: 16,
    letterSpacing: 0.15,
    color: nearlyWhite,
  );

  static TextStyle buildSubtitle1({Color color = darkText}) {
    return TextStyle(
      fontFamily: fontName,
      fontWeight: FontWeight.w500,
      fontSize: 16,
      letterSpacing: 0.15,
      color: color,
    );
  }

  static const TextStyle subtitle1Deactivated = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w500,
    fontSize: 16,
    letterSpacing: 0.15,
    color: deactivatedText,
  );

  static const TextStyle subtitle2 = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w500,
    fontSize: 14,
    letterSpacing: 0.1,
    color: darkerText,
  );

  static const TextStyle subtitle2Deactivated = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w500,
    fontSize: 14,
    letterSpacing: 0.1,
    color: deactivatedText,
  );

  static const TextStyle body1 = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.normal,
    fontSize: 16,
    letterSpacing: 0.5,
    color: darkerText,
  );
  static TextStyle buildBody1({Color color = AppTheme.darkText}) {
    return TextStyle(
      fontFamily: fontName,
      fontWeight: FontWeight.normal,
      fontSize: 16,
      letterSpacing: 0.5,
      color: color,
    );
  }


  static const TextStyle body2 = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.normal,
    fontSize: 14,
    letterSpacing: 0.25,
    color: darkerText,
  );

  static const TextStyle button = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w700,
    fontSize: 14,
    letterSpacing: 1.25,
    color: darkerText,
  );

  static TextStyle buildButton({Color color = AppTheme.darkText}) {
    return TextStyle(
      fontFamily: fontName,
      fontWeight: FontWeight.w700,
      fontSize: 14,
      letterSpacing: 1.25,
      color: color,
    );
  }

  static const TextStyle buttonLight = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w700,
    fontSize: 14,
    letterSpacing: 1.25,
    color: nearlyWhite,
  );

  static const TextStyle caption = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 13,
    letterSpacing: 0.4,
    color: darkerText,
  );

  static TextStyle buildCaption({Color color = darkText}) {
    return TextStyle(
      fontFamily: fontName,
      fontWeight: FontWeight.w400,
      fontSize: 13,
      letterSpacing: 0.4,
      color: color,
    );
  }

  static const TextStyle overline = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.normal,
    fontSize: 11,
    letterSpacing: 1.5,
    color: darkerText,
  );

  static const TextStyle tabBarLabelStyle = TextStyle(
    // h4 -> display1
    fontFamily: fontName,
    fontWeight: FontWeight.w500,
    fontSize: 16,
    letterSpacing: 0.4,
    color: darkerText,
  );

  static const TextStyle headline = TextStyle(
    // h5 -> headline
    fontFamily: fontName,
    fontWeight: FontWeight.w500,
    fontSize: 20,
    letterSpacing: 0.27,
    color: darkerText,
  );

  static const TextStyle title = TextStyle(
    // h6 -> title
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 18,
    letterSpacing: 0.18,
    color: darkerText,
  );

  static const TextStyle buttonTitleDarkText = TextStyle(
    // h6 -> title
    fontFamily: fontName,
    fontWeight: FontWeight.w600,
    fontSize: 15,
    letterSpacing: 0.3,
    color: darkerText,
  );

  static const TextStyle testSuiteHeaderText = TextStyle(
    // h6 -> title
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 24,
    letterSpacing: 0.18,
    color: darkerText,
  );
  static const TextStyle testSuiteBody1Text = TextStyle(
    // h6 -> title
    fontFamily: fontName,
    fontWeight: FontWeight.w300,
    fontSize: 14,
    letterSpacing: 0.18,
    color: darkerText,
  );

  static const TextStyle invitationUserNameText = TextStyle(
    // h6 -> title
    fontFamily: fontName,
    fontWeight: FontWeight.w500,
    fontSize: 16,
    letterSpacing: 0.1,
    color: darkerText,
  );

  static const TextStyle invitationSendAtText = TextStyle(
    // h6 -> title
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 13,
    letterSpacing: -0.5,
    color: deactivatedText,
  );

  static const TextStyle notificationTimeText = TextStyle(
    // h6 -> title
    fontFamily: fontName,
    fontWeight: FontWeight.w300,
    fontSize: 14.5,
    letterSpacing: -0.1,
    color: darkText,
  );

  static const TextStyle notificationDescriptionText = TextStyle(
    // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 15,
    letterSpacing: 0.1,
    color: dismissibleBackground,
  );

  static const TextStyle notificationHighlightText = TextStyle(
    // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w600,
    fontSize: 15.5,
    letterSpacing: 0.1,
    color: darkerText,
  );

  static const TextStyle invitationContentText = TextStyle(
    // h6 -> title
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 16,
    letterSpacing: -0.0,
    color: darkText,
  );

  static const TextStyle invitationExtendDetailLightText = TextStyle(
    // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 16,
    letterSpacing: -0.04,
    color: darkText,
  );

  static const TextStyle invitationExtendDetailText = TextStyle(
    // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 16,
    letterSpacing: -0.04,
    color: darkText,
  );

  static const TextStyle testSuiteParagraph2Text = TextStyle(
    // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w500,
    fontSize: 14,
    letterSpacing: -0.04,
    color: darkText,
  );

  static const TextStyle testSuiteParagraph2LightText = TextStyle(
    // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    letterSpacing: -0.04,
    color: darkText,
  );

  static const TextStyle testSuiteHintText = TextStyle(
    // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w300,
    fontSize: 16,
    letterSpacing: 0.5,
    color: dismissibleBackground,
  );

  static const TextStyle testSuiteSearchText = TextStyle(
    // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w500,
    fontSize: 18,
    letterSpacing: 0.5,
    color: darkText,
  );

  static const TextStyle testSuiteButtonTitleDarkText = TextStyle(
    // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 18,
    letterSpacing: 0.8,
    color: darkText,
  );

  static const TextStyle testSuiteButtonTitleLightText = TextStyle(
    // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 18,
    letterSpacing: 0.8,
    color: nearlyWhite,
  );

  static const TextStyle testSuiteSubtitleText = TextStyle(
    // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w300,
    fontSize: 14,
    letterSpacing: 0.5,
    color: dismissibleBackground,
  );
  static const TextStyle testSuiteSubtitleHighlightText = TextStyle(
    // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w500,
    fontSize: 14,
    letterSpacing: -0.04,
    color: dismissibleBackground,
  );

  static const TextStyle questionAnswerText = TextStyle(
    // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 16,
    letterSpacing: 0.2,
    color: darkText,
  );

  static const TextStyle invitationJobPositionItemText = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 18,
    letterSpacing: 0.7,
    color: darkText,
  );

  static const TextStyle invitationTitleItemText = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 17,
    letterSpacing: -0.1,
    color: darkText,
  );

  static const TextStyle invitationContentItemText = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 18,
    letterSpacing: 0.7,
    color: darkText,
  );

  static const TextStyle profileNameText = TextStyle(
    // h6 -> title
    fontFamily: fontName,
    fontWeight: FontWeight.w700,
    fontSize: 24,
    letterSpacing: 0.7,
    color: darkText,
  );
  static const TextStyle profileDescriptionText = TextStyle(
    // h6 -> title
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 16,
    letterSpacing: 0.0,
    color: deactivatedText,
  );

  static const TextStyle profileLableText = TextStyle(
    // h6 -> title
    fontFamily: fontName,
    fontWeight: FontWeight.w700,
    fontSize: 18,
    letterSpacing: 0.5,
    color: darkText,
  );
  static const TextStyle profileSubtitleText = TextStyle(
    // h6 -> title
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    letterSpacing: 0.5,
    color: deactivatedText,
  );

  static const TextStyle profileBodyHeavyLightText = TextStyle(
    // h6 -> title
    fontFamily: fontName,
    fontWeight: FontWeight.w600,
    fontSize: 16,
    letterSpacing: 0.5,
    color: nearlyWhite,
  );

  static const TextStyle profileBodyHeavyDarkText = TextStyle(
    // h6 -> title
    fontFamily: fontName,
    fontWeight: FontWeight.w600,
    fontSize: 16,
    letterSpacing: 0.5,
    color: darkText,
  );

  static const TextStyle profileHeader1DarkText = TextStyle(
    // h6 -> title
    fontFamily: fontName,
    fontWeight: FontWeight.w500,
    fontSize: 18,
    letterSpacing: -0.1,
    color: darkText,
  );

  static const TextStyle subtitle = TextStyle(
    // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    letterSpacing: -0.04,
    color: darkText,
  );

  // static const TextStyle body2 = TextStyle(
  //   // body1 -> body2
  //   fontFamily: fontName,
  //   fontWeight: FontWeight.w400,
  //   fontSize: 14,
  //   letterSpacing: 0.2,
  //   color: darkText,
  // );
  //
  // static const TextStyle body1 = TextStyle(
  //   // body2 -> body1
  //   fontFamily: fontName,
  //   fontWeight: FontWeight.w400,
  //   fontSize: 16,
  //   letterSpacing: -0.05,
  //   color: darkText,
  // );

  static const TextStyle body1Heavy = TextStyle(
    // body2 -> body1
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 16,
    letterSpacing: -0.05,
    color: darkText,
  );

  static const TextStyle body1_light = TextStyle(
    // body2 -> body1
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 16,
    letterSpacing: -0.05,
    color: lightText,
  );

// static const TextStyle caption = TextStyle(
//   // Caption -> caption
//   fontFamily: fontName,
//   fontWeight: FontWeight.w400,
//   fontSize: 12,
//   letterSpacing: 0.2,
//   color: lightText, // was lightText
// );
}
