import 'package:firebase_database/firebase_database.dart';
import 'package:seminar_demo/model/message_entity.dart';

class MessageRepository {
  static final MessageRepository _instance = MessageRepository._internal();

  factory MessageRepository() {
    return _instance;
  }

  MessageRepository._internal();

  final DatabaseReference _messagesRef = FirebaseDatabase(
          databaseURL:
              'https://dev-hunter-db07a-default-rtdb.asia-southeast1.firebasedatabase.app/')
      .reference()
      .child("message");

  void listenNewMessage({required Function(Message) onNewMessage}) {
    _messagesRef.onChildAdded.listen((event) {
      onNewMessage(Message().fromJson(event.snapshot.value));
    });
  }

  void sendMessage(Message message) {
    _messagesRef.push().set(message.toJson());
  }

  void getMessage() {
    _messagesRef.once().then((value) {
      print(value.value);
      print(value.key);
    });
  }


  Query getMessageQuery() {
    return _messagesRef;
  }
}
